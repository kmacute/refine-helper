<?php

namespace Kmacute\RefineHelper\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Kmacute\RefineHelper\Commands\MakeAuthentication;
use Kmacute\RefineHelper\Commands\MakeCrud;
use Kmacute\RefineHelper\Commands\MakeCrudController;
use Kmacute\RefineHelper\Commands\MakeModelController;

class RefineHelperServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerCommands();
        $this->registerRoutes();
    }

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                MakeCrudController::class,
                MakeAuthentication::class
            ]);
        }
    }

    protected function registerRoutes()
    {
        Route::group([
            'middleware' => [
                'api'
            ]
        ], function () {
            if (!file_exists(base_path('app/Domains'))) {
                return;
            };

            $domains = array_diff(scandir(base_path('app/Domains')), array('.', '..'));
            foreach ($domains as $domain) {
                $dirPath = base_path('app/Domains/') . $domain;

                if (!is_dir($dirPath)) {
                    continue;
                }

                $dirs = array_diff(scandir(base_path('app/Domains/' . $domain)), array('.', '..'));
                $routesDirExists = in_array('Routes', $dirs);
                if (!$routesDirExists) {
                    continue;
                }

                $domainRouteFiles = array_diff(scandir(base_path('app/Domains/' . $domain . '/Routes')), array('.', '..'));
                if (count($domainRouteFiles) == 0) {
                    continue;
                }

                Route::group([
                    'middleware' => [
                        'api'
                    ]
                ], function () use ($domainRouteFiles, $domain) {
                    foreach ($domainRouteFiles as $file) {
                        $this->loadRoutesFrom(base_path('app/Domains/' . $domain . '/Routes/' . $file));
                    }
                });
            }
        });
    }
}

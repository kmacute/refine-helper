<?php

namespace Kmacute\RefineHelper\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class QueryFilters
{
    /**
     * The request instance.
     * Integration of Refine.Dev
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Create a new QueryFilters instance.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply the requested filters to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Builder $query)
    {
        $this->handleFilters($query);
        $this->handlePagination($query);
        $this->handleSorting($query);
        $this->handleSelect($query);
        $this->handleIds($query);

        return $query;
    }

    /**
     * Apply filters and select to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function handleFilters(Builder $query)
    {
        if (!$this->request->get('_filters')) return $query;

        $stringifiedFilter = $this->request->get('_filters');
        $filters = json_decode($stringifiedFilter, true);

        foreach ($filters as $filter) {
            $field = $filter['field'];
            $operator = $filter['operator'];
            $value = $filter['value'];

            if (!$value) continue;

            switch ($operator) {
                case 'eq':
                    $query->where($field, $value);
                    break;
                case 'neq':
                    $query->where($field, '!=', $value);
                    break;
                case 'startswith':
                    $query->where($field, 'like', "$value%");
                    break;
                case 'endswith':
                    $query->where($field, 'like', "%$value");
                    break;
                case 'contains':
                    $query->where($field, 'like', "%$value%");
                    break;
                case 'in':
                    $query->whereIn($field, $value);
                    break;
            }
        }

        return $query;
    }

    /**
     * Apply pagination to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function handlePagination(Builder $query)
    {
        if ($this->request->has('_current')) {
            $pageSize = $this->request->input('_pageSize', 15); // Default page size is 15
            $currentPage = $this->request->input('_current');
            $skip = ($currentPage - 1) * $pageSize;

            $query->skip($skip)->take($pageSize);
        }

        return $query;
    }

    /**
     * Apply sorting to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function handleSorting(Builder $query)
    {
        if ($this->request->has('_sort')) {
            $sortField = $this->request->input('_sort');
            $sortOrder = $this->request->input('_order', 'asc');

            $query->orderByRaw("{$sortField} {$sortOrder}");
        }

        return $query;
    }

    /**
     * Apply select fields to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function handleSelect(Builder $query)
    {
        if ($this->request->has('_fields')) {
            $query->select(json_decode($this->request->get('_fields'), true));
        }

        return $query;
    }

    /**
     * Apply filtering by id to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function handleIds(Builder $query)
    {
        if ($this->request->has('_id')) {
            $query->whereIn('id', $this->request->get('_id')); // for select purposes
        }

        return $query;
    }
}

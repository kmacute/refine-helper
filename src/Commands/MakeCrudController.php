<?php

namespace Kmacute\RefineHelper\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class MakeCrudController extends Command
{
    protected $modelName;
    protected $modelNamePluralLowerCase;
    protected $modelNameSingularLowerCase;
    protected $modelNamePlural;
    protected $modelNameSingular;
    protected $validationFields;
    protected $fillableFields;
    protected $defaultDataFields;
    protected $updateDataFields;
    protected $assertDataFields;
    protected $domain;

    protected $signature = 'refine:make:crud
                            {name : Class (singular) for example User}
                            {--domain= : Add domain}
                            {--route : Add route}
                            {--force : This will force replace the records}';

    protected $description = 'Create CRUD base on database first approach';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $nameArgument = $this->argument('name');
        $this->domain = $this->option('domain') ? $this->option('domain') : '';

        $this->modelName = Str::studly(Str::singular($nameArgument));
        $this->modelNamePluralLowerCase = Str::snake(Str::plural(strtolower($nameArgument)));
        $this->modelNameSingularLowerCase = Str::snake(strtolower($nameArgument));
        $this->modelNamePlural = Str::studly(Str::plural($nameArgument));
        $this->modelNameSingular = Str::studly(Str::singular($nameArgument));

        $database = env('DB_DATABASE', '');
        $real_name = strtolower($nameArgument);
        $columns = DB::select("
            SELECT COLUMN_NAME, Column_Default, Column_Key, IS_NULLABLE, DATA_TYPE,
            COALESCE(Character_Maximum_Length, Numeric_Precision) AS Column_Size
            FROM INFORMATION_SCHEMA.Columns
            WHERE table_name = '$real_name' AND table_schema = '$database'");

        $this->generateFields($columns);
        $this->createFiles();
    }

    protected function generateFields($columns)
    {
        $this->validationFields = $this->generateValidation($columns);
        $this->fillableFields = $this->generateFillableFields($columns);
        $this->defaultDataFields = $this->generateDefaultDataFields($columns);
        $this->updateDataFields = $this->generateUpdateDataFields($columns);
        $this->assertDataFields = $this->generateAssertDataFields($columns);
    }

    protected function createFiles()
    {
        $base_path_model = 'Models';
        $base_path_controller = 'Http/Controllers/API';
        $base_path_request = 'Http/Requests';
        $base_path_route = '';


        if ($this->domain) {
            $base_path_controller = "Domains/{$this->domain}/Features/{$this->modelNameSingular}/Controllers";
            $base_path_model = "Domains/{$this->domain}/Features/{$this->modelNameSingular}/Models";
            $base_path_request = "Domains/{$this->domain}/Features/{$this->modelNameSingular}/Requests";
            $base_path_route = "Domains/{$this->domain}/Routes";
        }

        $this->createFile('model', $base_path_model, $this->modelName, 'Model', [
            '{{modelName}}' => $this->modelName,
            '{{modelNamePluralLowerCase}}' => $this->modelNamePluralLowerCase,
            '{{fillableFields}}' => $this->fillableFields,
            'Models' => str_replace("/", "\\", $base_path_model)
        ]);

        $this->createFile('controller', $base_path_controller, "{$this->modelName}Controller", 'Controller', [
            '{{modelName}}' => $this->modelName,
            '{{modelNamePluralLowerCase}}' => $this->modelNamePluralLowerCase,
            '{{modelNameSingularLowerCase}}' => $this->modelNameSingularLowerCase,
            'Http\Controllers\API' => str_replace("/", "\\", $base_path_controller),
            'Models' => str_replace("/", "\\", $base_path_model),
            'Http\Requests' =>  str_replace("/", "\\", $base_path_request),
        ]);

        $this->createFile('request', $base_path_request, "{$this->modelName}Request", 'Request', [
            '{{modelName}}' => $this->modelName,
            '{{fields}}' => $this->validationFields,
            'Http\Requests' =>  str_replace("/", "\\", $base_path_request),
        ]);

        if ($this->domain) {
            if ($this->option('route')) {
                $this->createFile('request', $base_path_route, "{$this->modelName}Route", 'Route', [
                    '{{modelName}}' => $this->modelName,
                    '{{modelNamePluralLowerCase}}' => $this->modelNamePluralLowerCase,
                    'Http\Controllers\API' => str_replace("/", "\\", $base_path_controller),
                ]);
            }
        } else {
            File::append(
                base_path('routes/api.php'),
                "Route::resource('{$this->modelNamePluralLowerCase}', App\\Http\\Controllers\\API\\{$this->modelName}Controller::class);\r\n"
            );
        }
    }

    protected function createFile($type, $directory, $fileName, $stubType, $replacements)
    {
        $template = str_replace(array_keys($replacements), array_values($replacements), $this->getStub($stubType));
        $path = app_path("{$directory}/{$fileName}.php");

        if (file_exists($path) && !$this->option('force')) {
            echo ("{$fileName}.php already exists! \r\n");
            return;
        }

        if (!file_exists($directoryPath = app_path($directory))) {
            mkdir($directoryPath, 0777, true);
        }

        file_put_contents($path, $template);
        echo ("{$fileName}.php created! \r\n");
    }

    protected function getStub($type)
    {
        return file_get_contents(__DIR__ .  "/Stubs/$type.stub");
    }

    private function generateFillableFields($fields)
    {
        return $this->generateFieldsBasedOnColumns($fields, function ($column) {
            return "'{$column->COLUMN_NAME}'";
        });
    }

    private function generateValidation($fields)
    {
        return $this->generateFieldsBasedOnColumns($fields, function ($column) {
            $rules = [$column->IS_NULLABLE === 'YES' ? "'nullable'" : "'required'"];
            if ($column->DATA_TYPE === 'varchar' || $column->DATA_TYPE === 'char') {
                $rules[] = "'string'";
                $rules[] = "'max:{$column->Column_Size}'";
            } elseif ($column->DATA_TYPE === 'decimal' || $column->DATA_TYPE === 'int') {
                $rules[] = "'numeric'";
            } elseif ($column->DATA_TYPE === 'tinyint') {
                $rules[] = "'boolean'";
            }

            return "'{$column->COLUMN_NAME}' => [" . implode(', ', $rules) . ']';
        }, 12);
    }

    private function generateDefaultDataFields($fields)
    {
        return $this->generateDataFields($fields, 'a', '1', '2022-01-01 00:00:00', '2022-01-01');
    }

    private function generateUpdateDataFields($fields)
    {
        return $this->generateDataFields($fields, 'b', '2', '2022-01-02 00:00:00', '2022-01-02');
    }

    private function generateAssertDataFields($fields)
    {
        return $this->generateFieldsBasedOnColumns($fields, function ($column) {
            return "\$this->assertEquals('2', \${$this->modelName}::first()->{$column->COLUMN_NAME});";
        });
    }

    private function generateFieldsBasedOnColumns($fields, callable $callback, $space = 8)
    {
        $filteredFields = array_filter($fields, function ($column) {
            return !in_array($column->COLUMN_NAME, ['id', 'created_by', 'updated_by', 'deleted_by', 'created_at', 'updated_at', 'deleted_at']);
        });

        $indentation = str_repeat(' ', $space);
        return implode(",\r\n{$indentation}", array_map($callback, $filteredFields));
    }

    private function generateDataFields($fields, $stringVal, $numericVal, $dateTimeVal, $dateVal)
    {
        return $this->generateFieldsBasedOnColumns($fields, function ($column) use ($stringVal, $numericVal, $dateTimeVal, $dateVal) {
            $value = match ($column->DATA_TYPE) {
                'varchar', 'char' => $stringVal,
                'decimal', 'int' => $numericVal,
                'datetime' => $dateTimeVal,
                'date' => $dateVal,
                default => ''
            };
            return "'{$column->COLUMN_NAME}' => '{$value}'";
        });
    }
}

<?php

namespace Kmacute\RefineHelper\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class MakeAuthentication extends Command
{
    protected $modelName;
    protected $modelNamePluralLowerCase;
    protected $modelNameSingularLowerCase;
    protected $modelNamePlural;
    protected $modelNameSingular;
    protected $validationFields;
    protected $fillableFields;
    protected $defaultDataFields;
    protected $updateDataFields;
    protected $assertDataFields;

    protected $signature = 'refine:make:auth
    {--force : This will force replace the records}
    {--route : This add route to api routes}';

    protected $description = 'Create authentication';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->createFiles();
    }

    protected function createFiles()
    {
        $this->createFile('Domains/Auth/Features/Authentication/Controllers', "AuthenticationController", 'AuthenticationController', []);
        $this->createFile('Domains/Auth/Features/Authentication/Requests', "ChangePasswordRequest", 'ChangePasswordRequest', []);
        $this->createFile('Domains/Auth/Features/Authentication/Requests', "LoginRequest", 'LoginRequest', []);

        if ($this->option('route')) {
            $this->createFile("Domains/Auth/Routes", "AuthenticationRoute", 'AuthenticationRoute', []);
        }
    }

    protected function createFile($directory, $fileName, $stubName, $replacements)
    {
        $template = str_replace(array_keys($replacements), array_values($replacements), $this->getStub($stubName));
        $path = app_path("{$directory}/{$fileName}.php");

        if (file_exists($path) && !$this->option('force')) {
            echo ("{$fileName}.php already exists! \r\n");
            return;
        }

        if (!file_exists($directoryPath = app_path($directory))) {
            mkdir($directoryPath, 0777, true);
        }

        file_put_contents($path, $template);
        echo ("{$fileName}.php created! \r\n");
    }

    protected function getStub($type)
    {
        return file_get_contents(__DIR__ .  "/AuthStubs/$type.stub");
    }
}

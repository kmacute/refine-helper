<?php

namespace App\Domains\Auth\Features\Authentication\Controllers;

use App\Http\Controllers\API\Authentication\Requests\ChangePasswordRequest;
use App\Http\Controllers\API\Authentication\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function login(LoginRequest $request)
    {
        /** @var User*/
        $username = User::where('username', $request->username)->firstOrFail();

        return [
            'token' => $username->createToken(request()->ip())->plainTextToken
        ];
    }

    public function me()
    {
        return Auth::user();
    }

    /**
     * Change the user password
     * 
     * @param \App\Http\Requests\ChangePasswordRequest $request
     * @return \App\Models\User
     */
    public function change_password(ChangePasswordRequest $request)
    {
        /** @var User */
        $user = Auth::user();
        $user->password = encrypt($request->password);
        $user->save();
        return $user;
    }

    public function encryption_key()
    {
        return env('LOGIN_KEY');
    }
}
